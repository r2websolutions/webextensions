export default class BaseError extends Error 
{
	constructor(message) {
		super(message);
		this.name = this.constructor.name;
	}

	toString() {
		return this.name;
	}
}