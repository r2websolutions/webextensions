import Component from './Component';


export default class Application {
	constructor(config = {}) {
		this.EVENT_BEFORE_REQUEST = 'beforeRequest';
		this.EVENT_AFTER_REQUEST = 'afterRequest';
		this.STATE_BEGIN = 0;
		this.STATE_INIT = 1;
		this.STATE_BEFORE_REQUEST = 2;
		this.STATE_HANDLING_REQUEST = 3;
		this.STATE_AFTER_REQUEST = 5;
		this.STATE_END = 5;

		this.controllerNamespace = 'src/controllers';
		this.requestedRoute;
		this.requestedAction;
		this.requestedParams;

		this.bootstrap = [];

		this.state;

		this.loadedModules;

		window['We'] = this;

		this.state = this.STATE_BEGIN;
		this.preInit(config);

		this.registerErrorHandler(config);
		
		Component;
	}

	bootstrap() {
		// for(let )
	}
}