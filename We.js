import Logger from './log/Logger';
import InvalidConfigError from './base/InvalidConfigError';

// the application time starts here
performance.mark('START_TIME');

/**
 * @type {WeakMap}
 * @private
 */
let _logger = new WeakMap();

/**
 * We is a special helper class that is used to carry objects through the entire request
 */
export default class We {

	/**
	 * @returns {Logger} - The message logger object
	 */
	static get logger() {
		if(_logger.get(this) === undefined) {
			_logger.set(this, new Logger);
		}

		return _logger.get(this);
	}

	/**
	 * Set the logger object
	 * 
	 * @param {Logger} logger - The logger object
	 */
	static set logger(logger) {
		if(!(logger instanceof Logger)) {
			throw new InvalidConfigError(`Logger configuration must be an instance of Logger`);
		}
		_logger.set(this, logger);
	}

	/**
	 * 
	 * @param {string|string[]} message - The message(s) to be logged
	 * @param {string} category - The category of the message
	 */
	static debug(message, category = 'application') {
		this.logger.log(message, Logger.LEVEL_TRACE, category);
	}

	/**
	 * 
	 * @param {string|string[]} message - The message(s) to be logged
	 * @param {string} category - The category of the message
	 */
	static error(message, category = 'application') {
		this.logger.log(message, Logger.LEVEL_ERROR, category);
	}

	/**
	 * 
	 * @param {string|string[]} message - The message(s) to be logged
	 * @param {string} category - The category of the message
	 */
	static warning(message, category = 'application') {
		this.logger.log(message, Logger.LEVEL_WARNING, category);
	}

	/**
	 * 
	 * @param {string|string[]} message - The message(s) to be logged
	 * @param {string} category - The category of the message
	 */
	static info(message, category = 'application') {
		this.logger.log(message, Logger.LEVEL_INFO, category);
	}
}